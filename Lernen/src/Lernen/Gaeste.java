package Lernen;
import java.util.Scanner;


	public class Gaeste {
		public static int gaeste = 3;
		
		private static int GetData() {
			return gaeste;
		}
		public static void main (String[] args) {
			int temp_guestdata = GetData();
			int uebergabe;
			int eingabe;
			int abfrage;
			try (Scanner scan = new Scanner(System.in)) {
				boolean weiter = true;
				
				System.out.println("Bitte gib die Anzahl der aktuell angemeldeten Gäste ein");
				temp_guestdata = scan.nextInt();
				
				while(weiter) {
					System.out.println("Gast anmelden(1) Gast abmelden(2) Programm beenden(3)");
					eingabe = scan.nextInt();
					
					if(eingabe == 1) {
						System.out.println("Wie viele Gäste möchtest du anmelden?");
						uebergabe = scan.nextInt();
						temp_guestdata = temp_guestdata + uebergabe;
						System.out.println("Du hast " + uebergabe + " Gäste angemeldet");
						System.out.println("Es sind jetzt " + temp_guestdata + " angemeldet");
					}
					
					if(eingabe == 2) {
						System.out.println("Wie viele Gäste möchtest du abmelden?");
						uebergabe = scan.nextInt();
						temp_guestdata = temp_guestdata - uebergabe;
						System.out.println("Du hast " + uebergabe + " Gäste abgemeldet");
						System.out.println("Es sind jetzt " + temp_guestdata + " angemeldet");
					}
					
					if(eingabe == 3) {
						System.out.println("Möchtest du das Programm wirklich beenden? Ja(1), Nein(2)");
						abfrage = scan.nextInt();
						
						if(abfrage == 1) {
							System.out.println("Das Programm wurde beendet!");
							weiter = false;
							
						}
						
					}
				}
			}
			
		}
	}
	