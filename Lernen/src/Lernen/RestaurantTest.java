package Lernen;

/*
 * unused
 * 
import java.awt.Panel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
*/

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;



public class RestaurantTest {
	
	/*
	 * Test of entered Values
	 */
	static boolean validate(int value, int min, int max) {
		return min <= value && value <= max;
	}
	
	
	private JFrame frame;
	public int allguests;
	public int dialogResult;
	
	/*
	 * Sign Panel
	 */
	private JTextField textField_sign;
	private JButton btnNewButton_sign;
	
	/*
	 * 	Unsign Panel
	 */
	private JTextField textField_unsign;
	private JButton btnNewButton_unsign;
	
	/*
	*	MainFrame
	*/
	private JButton button_sign;
	private JButton button_unsign;
	private JButton button_close;
	private static JLabel guests_overview;
	
	/*
	 * 	Cache Panels
	 */
	private static JPanel currentsignpanel;
	private static JPanel mainpanel;
	private static JPanel currentunsignpanel;

	
	
	public void CreateMain() {
		JPanel temppanel_main = new JPanel();
		temppanel_main.setBounds(0, 0, 450, 300);
		temppanel_main.setLayout(null);
		temppanel_main.setVisible(true);
		JLabel tempguests_overview = new JLabel("Aktuell sind " + allguests + " Gäste angemeldet");
		tempguests_overview.setHorizontalAlignment(SwingConstants.CENTER);
		tempguests_overview.setBounds(110, 11, 210, 14);
		button_sign = new JButton("Gäste Anmelden");
		button_sign.setBounds(27, 227, 120, 23);
		button_sign.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				currentsignpanel.setVisible(true);
				mainpanel.setVisible(false);
			}
		});
		button_unsign = new JButton("Gäste Abmelden");
		button_unsign.setBounds(157, 227, 120, 23);
		button_unsign.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				currentunsignpanel.setVisible(true);
				mainpanel.setVisible(false);
			}
		});
		button_close = new JButton("Prgramm beenden");
		button_close.setBounds(284, 227, 120, 23);
		button_close.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				dialogResult = JOptionPane.showConfirmDialog(null, "Möchtest du das Programm wirklich beenden", "Beenden?", dialogResult, JOptionPane.YES_NO_OPTION);
				if(dialogResult == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
		});
		temppanel_main.add(tempguests_overview);
		temppanel_main.add(button_sign);
		temppanel_main.add(button_unsign);
		temppanel_main.add(button_close);
		mainpanel = temppanel_main;
		guests_overview = tempguests_overview;
	}
	
	public void CreateAbmelden() {
		JPanel temppanel_unsign = new JPanel ();
		temppanel_unsign.setBounds(0, 0, 450, 300);
		temppanel_unsign.setLayout(null);
		temppanel_unsign.setVisible(false);
		JLabel unsign_line1 = new JLabel("Bitte gibt die Anzahl der Gäste ein,");
		unsign_line1.setHorizontalAlignment(SwingConstants.CENTER);
		unsign_line1.setBounds(110, 11, 210, 14);
		JLabel unsign_line2 = new JLabel("die du anmelden möchtest");
		unsign_line2.setHorizontalAlignment(SwingConstants.CENTER);
		unsign_line2.setBounds(120, 37, 210, 14);
		textField_unsign = new JTextField();
		textField_unsign.setBounds(177, 62, 86, 20);
		textField_unsign.setColumns(10);
		JLabel output_unsign = new JLabel();
		output_unsign.setBounds(200, 11, 210, 14);
		output_unsign.setVisible(false);
		
		btnNewButton_unsign = new JButton("Abmelden");
		btnNewButton_unsign.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
					output_unsign.setText(textField_unsign.getText());
					String tempsguests = output_unsign.getText();
					int uguests = Integer.parseInt(tempsguests);
					final int min = 1;
					final int max = allguests;
					if( !validate(uguests, min, max) ) {
						if(allguests == 0 && uguests == 0) {
							JOptionPane.showMessageDialog(output_unsign, "Es ist aktuell kein Gast angemeldet \n Du kannst keinen Gast abmelden!");
						}
						else if(allguests == 1) {
							JOptionPane.showMessageDialog(output_unsign, "Du hast eine falsche Anzahl eingegeben! \n Aktuell befindet sich " + allguests + " Gast in deinem Restaurant. \n Du musst mindestens " + min + " eingeben. \n Deine Eingabe darf aber nicht gößer als " + max + " sein. \n  Bitte überprüfe deine Eingabe!");
						}
						else {
							JOptionPane.showMessageDialog(output_unsign, "Du hast eine falsche Anzahl eingegeben! \n Aktuell befinden sich " + allguests + " Gäste in deinem Restaurant. \n Du musst mindestens " + min + " eingeben. \n Deine Eingabe darf aber nicht gößer als " + max + " sein. \n  Bitte überprüfe deine Eingabe!");
						}
					}
					else {
						allguests = allguests - uguests;
						if(uguests == 1) {
							JOptionPane.showMessageDialog(output_unsign, "Du hast " + uguests + " Gast erfolgreich abgemeldet");
						}
						else {
							JOptionPane.showMessageDialog(output_unsign, "Du hast " + uguests + " Gäste erfolgreich abgemeldet");
						}
					}
					temppanel_unsign.setVisible(false);
					textField_unsign.setText("");
					guests_overview.setText("Aktuell sind " + allguests + " Gäste angemeldet");
					mainpanel.setVisible(true);
					
			}
		});
		btnNewButton_unsign.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_unsign.setBounds(177, 227, 100, 20);
		btnNewButton_unsign.setHorizontalAlignment(SwingConstants.CENTER);
		temppanel_unsign.add(unsign_line1);
		temppanel_unsign.add(unsign_line2);
		temppanel_unsign.add(textField_unsign);
		temppanel_unsign.add(btnNewButton_unsign);
		temppanel_unsign.add(output_unsign);
		currentunsignpanel = temppanel_unsign;
	}	
	
	
	public void CreateAnmelden() {
		JPanel temppanel_sign = new JPanel ();
		temppanel_sign.setBounds(0, 0, 450, 300);
		temppanel_sign.setLayout(null);
		temppanel_sign.setVisible(false);
		JLabel sign_line1 = new JLabel("Bitte gibt die Anzahl der Gäste ein,");
		sign_line1.setHorizontalAlignment(SwingConstants.CENTER);
		sign_line1.setBounds(110, 11, 210, 14);
		JLabel sign_line2 = new JLabel("die du anmelden möchtest");
		sign_line2.setHorizontalAlignment(SwingConstants.CENTER);
		sign_line2.setBounds(120, 37, 210, 14);
		textField_sign = new JTextField();
		textField_sign.setBounds(177, 62, 86, 20);
		textField_sign.setColumns(10);
		JLabel output_sign = new JLabel();
		output_sign.setBounds(200, 11, 210, 14);
		output_sign.setVisible(false);
		
		btnNewButton_sign = new JButton("Anmelden");
		btnNewButton_sign.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
					output_sign.setText(textField_sign.getText());
					String tempsguests = output_sign.getText();
					int sguests = Integer.parseInt(tempsguests);
					final int min = 1;
					final int max = Integer.MAX_VALUE-allguests;
					if( !validate(sguests, min, max) ) {
						JOptionPane.showMessageDialog(output_sign, "Du hast eine falsche Anzahl eingegeben! \n Du musst mindestes " + min + " eingeben! \n Deine Eingabe war " + sguests + "\n Bitte überprüfe deine Eingabe!");
					}
					else {
						allguests = allguests + sguests;
						if(sguests == 1) {
							JOptionPane.showMessageDialog(output_sign, "Du hast " + sguests + " Gast erfolgreich angemeldet");
						}
						else {
							JOptionPane.showMessageDialog(output_sign, "Du hast " + sguests + " Gäste erfolgreich angemeldet");
						}
					}
					temppanel_sign.setVisible(false);
					textField_sign.setText("");
					guests_overview.setText("Aktuell sind " + allguests + " Gäste angemeldet");
					mainpanel.setVisible(true);
					
			}
		});
		btnNewButton_sign.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_sign.setBounds(177, 227, 100, 20);
		btnNewButton_sign.setHorizontalAlignment(SwingConstants.CENTER);
		temppanel_sign.add(sign_line1);
		temppanel_sign.add(sign_line2);
		temppanel_sign.add(textField_sign);
		temppanel_sign.add(btnNewButton_sign);
		temppanel_sign.add(output_sign);
		currentsignpanel = temppanel_sign;
	}	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RestaurantTest window = new RestaurantTest();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RestaurantTest() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		CreateAnmelden();
		CreateMain();
		CreateAbmelden();
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
		frame.getContentPane().add(currentsignpanel);
		frame.getContentPane().add(currentunsignpanel);
		frame.getContentPane().add(mainpanel);	
	}
}
